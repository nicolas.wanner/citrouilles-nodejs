'use strict';

var express = require('express');
var router = express.Router();

// Les sprites à afficher
var spriteList  = null;

const MIN_X = 0;
const MAX_X = 500;

const MIN_Y = 0;
const MAX_Y = 500;

const STEP = 10;

function initSession(req) {
  if (!req.session.spriteList) {
    req.session.spriteList = [
      {id: 0, x:   0, y:   0, name: 'Gérard'   },
      {id: 1, x: 100, y: 120, name: 'Liliane'  },
      {id: 2, x: 200, y: 140, name: 'Géraldine'},
    ];  
  }
  spriteList = req.session.spriteList;
}

// Affiche la home page toute bête
router.get('/', function(req, res, next) {
  initSession(req);
  res.render('index', { title: 'Express', spriteList : spriteList, spriteId : 0});
});

// Affiche la home page avec le formulaire
router.post('/', function(req, res, next) {
  let action   = req.body.action;
  let spriteId = req.body.spriteId;

  initSession(req);

  console.log(spriteList);


  // Seulement si on est dans le tableau
  if ((spriteId >= 0) && (spriteId < spriteList.length)) {
    let sprite = spriteList[spriteId];
    // Bouger selon demande
    switch(action) {
      case 'addSprite' : console.log('Ajouter'); res.redirect('/edit'); break;
      case 'left'      : sprite.x = Math.max(MIN_X, sprite.x - STEP); break;
      case 'right'     : sprite.x = Math.min(MAX_X, sprite.x + STEP); break;
      case 'up'        : sprite.y = Math.max(MIN_Y, sprite.y - STEP); break;
      case 'down'      : sprite.y = Math.min(MAX_Y, sprite.y + STEP); break;
    }
  }
  else {
    // Erreur dans l'ID, reprend le 1er
    spriteId = 0;
  }
  res.render('index', { title: 'Halloween', spriteList : spriteList, spriteId : spriteId});
});

// Affiche le formulaire d'ajout d'un sprite
router.get('/edit', function(req, res, next) {
  initSession(req);
  res.render('edit', { title: 'Halloween', name : '', x: '', y: ''});
});

// Edition d'un sprite et affiche le formulaire
router.post('/edit', function(req, res, next) {
  initSession(req);

  let action = req.body.action;
  let sprite = {
    id:   -1,
    x:    parseInt(req.body.x),
    y:    parseInt(req.body.y),
    name: req.body.name
  };    

  if (action == 'cancel') {
    res.redirect('/');
    return;
  }
  else if (action == 'validate') {

    if ((sprite.x > MIN_X) && (sprite.x < MAX_X) && (sprite.y > MIN_Y) && (sprite.y < MAX_Y) && sprite.name != '') {
      sprite.id = spriteList.length;
      spriteList.push(sprite);
      res.redirect('/');
      return;
    }
  }

  console.log(sprite);
  res.render('edit', { title: 'Halloween', name : sprite.name, x: sprite.x, y: sprite.y});
});

module.exports = router;
