# citrouilles-nodejs

Ce programme est un petit essai que j'ai réalisé pour tester l'utilisation de nodejs avec express et express-generator pour réaliser une application web qui a été demandées aux élèves sous la forme d'une application PHP.

L'utilisation de pug comme outils de template n'est pas adaptée au public

Le site est fonctionnel mais je trouve que son écriture demande plus de connaissances et compétences que l'équivalent en PHP


## Utilisation après clonage du projet

Pour installer le projet depuis un dépôts

```bash
git clone https://gitlab.com/nicolas.wanner/citrouilles-nodejs.git
cd citrouilles-nodejs
npm install
```

Démarrer le serveur

```bash
DEBUG=citrouilles-nodejs:* npm start
```

Tester dans un navigateur

http://localhost:3000

## Création de l'application

Avant de commencer, j'ai installé globalement le générateur de site express

```bash
npm install -g express-generator
```

Ensuite il faut faire appel au générateur de squelette d'application pour fabriquer l'arborescence de fichier

```bash
express --view=pug <nom-du-projet>
```

J'ai pas exploré les autres outils de templating mais pug utilise des fichiers très court mais difficile d'accès pour les élèves.

Installer les modules nécessaires pour faire fonctionner le projets

```bash
npm install
npm install express-session
npm install connect-sqlite3
npm install cookie-parser
```

puis pour démarrer le serveur

```bash
DEBUG=<nom-du-projet>:* npm start
```
